# -*- coding: utf-8 -*-
{
    'name': "Nieve Layout",

    'summary': """
    """,

    'description': """

    """,

    'author': "NIEVE",
    'website': "http://www.nievetechnology.com/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'views/layout_template.xml',

    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}